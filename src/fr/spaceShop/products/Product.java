package fr.spaceShop.products;

public class Product {
	private String type ;
	private String name ; 
	private double priceWholeSealer ;
	private double price; 
	private String mark ; 
	private int id ; 
	private int stock = 0 ;
	private static int nbProduct = 0 ;


	
	public Product(String name, double priceWholeSealer, String type, String mark) {
		this.name = name ; 
		this.priceWholeSealer = priceWholeSealer ; 
		this.price = priceWholeSealer ; 
		id= nbProduct++; 
		this.type = type ; 
		this.mark = mark ; 
		
		
	}
	
	void setPrice(double price){
		this.price = price ; 
		
	}
	
	void setPriceWholeSealer(double priceWholeSealer){
		this.priceWholeSealer = priceWholeSealer; 
	}
	
	void setStock(int stock){
		this.stock = stock ; 
	}
	
	public double getPrice(){
		return this.price ;
	}
	
	public double getPriceWholeSealer(){
		return this.priceWholeSealer ;
	}
	
	public String getName(){
		return this.name ;
	}
	
	public String getType(){
		return this.type ;
	}
	
	public String getMark(){
		return this.mark ;
	}
	
	public int getID(){
		return this.id ;
	}
	
	public boolean isInStock(){
		return stock>0 ; 
	}
	
	

}
