package fr.spaceShop.globalInformations;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class Date implements Serializable{
	public static Date currentDate = null;
	private int day;
	private int month;
	private int year;
	
	public static void init(){
		currentDate = new Date();
	}
	
	public Date(){
		day = 12;
		month = 12;
		year = 9812;
	}
	
	public Date(int day, int month, int year){
		this.day = day;
		this.month = month;
		this.year = year;
	}
	
	public int getDay(){
		return day;
	}
	
	public int getMonth(){
		return month;
	}
	
	public int getYear(){
		return year;
	}
	
	public boolean isBefore(Date date2){
		if(year < date2.getYear()) return true;
		else if (year > date2.getYear()) return false;
		else {
			if(month < date2.getMonth()) return true;
			else if (month > date2.getMonth()) return false;
			else {
				if(day < date2.getDay()) return true;
				else return false;
			}
		}
	}
	
	public boolean isSame(Date date2){
		if(year == date2.getYear() && month == date2.getMonth() && day == date2.getDay()) return true;
		else return false;
	}
	
	public static boolean nextDay(){ //bug sur décembre
		boolean leapYear = false;
		if(currentDate.year%4 == 0 || (currentDate.year%100 == 0 && currentDate.year%400 != 0)){
			leapYear = true;
		}
		if(currentDate.day < 25){
			currentDate.day += 1;
			return false;
		}
		else if(currentDate.month==2 &&((leapYear  && currentDate.day == 25)||(!leapYear  && currentDate.day == 27))){
			currentDate.day = 1;
			currentDate.month =3;
			return true;
		}
		else if(currentDate.day == 30 && (currentDate.month == 2 || currentDate.month == 4 || currentDate.month == 6 || currentDate.month == 8 ||currentDate.month == 10)){
			currentDate.day +=1;
			return false;
		}
		else if(currentDate.day < 31) {
			currentDate.day += 1;
			return false;
		}
		else if(currentDate.day == 31 && currentDate.month == 12){
			currentDate.day =1;
			currentDate.month = 1;
			currentDate.year += 1;
			return true;
		}
		
		else{
			currentDate.day = 1;
			currentDate.month += 1;
			return true;
		}
	}
	
	public static String currentDateToString(){
		return currentDate.day + "/" + currentDate.month + "/" + currentDate.year;
	}

	public static void load() {
		ObjectInputStream ois;
    	try {
    		ois = new ObjectInputStream(
    	              new BufferedInputStream(
    	                new FileInputStream(
    	                  new File("files/gameDate"))));
    	            

    	      try {
    	    	  currentDate =(Date)ois.readObject();
    	      } catch (ClassNotFoundException e) {
    	        e.printStackTrace();
    	      }
    	      ois.close();
    	    } catch (FileNotFoundException e) {
    	      e.printStackTrace();
    	    } catch (IOException e) {
    	      e.printStackTrace();
    	    } 
		
	}
}
