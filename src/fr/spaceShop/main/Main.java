package fr.spaceShop.main;

import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.MouseAdapter;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;

import javax.swing.JButton;
import javax.swing.JFrame;

import fr.spaceShop.globalInformations.Date;
import fr.spaceShop.gui.Window;
import fr.spaceShop.humans.Human;
import fr.spaceShop.shop.Shop;
import fr.spaceShop.humans.PDG;
import fr.spaceShop.time.Time;

public class Main {
	static Window window = new Window();
	public static Shop shop; 

	public static void main(String[] args) {
	}
	
	public static void newGame(){
		window.repaint();
		Human.init();
		Date.init();
		shop = new Shop();
		PDG pdg = new PDG();
		shop.addWorker(pdg);
		Time.gameLoop();
	}
	
	public static void load(){
		Human.init();
		Date.load();
		ObjectInputStream ois;
		//get name
		try {
			ois = new ObjectInputStream(new BufferedInputStream(
					new FileInputStream(new File("files/shop"))));

			try {
				shop = (Shop) ois.readObject();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
			ois.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}	
	}
}
