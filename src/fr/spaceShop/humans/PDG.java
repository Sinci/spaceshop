package fr.spaceShop.humans;

import fr.spaceShop.main.Main;
import fr.spaceShop.shop.Shop;

public class PDG extends Worker{
	private static float money;

	public PDG(){
		super();
	}
	
	public String getJob() {
		return "PDG";
	}
	
	public static void addToMoney(int sum){
		money += sum;
	}
	
	
	public void steal() {
		int sumSteal;
		if(Main.shop.getMoney()>10) sumSteal = (int) -(Math.random() * ( 10));
		else sumSteal = (int) -(Math.random() * ( Main.shop.getMoney()));
		Main.shop.addToMoney(sumSteal);
		money += sumSteal;
	}
	
	public void trySteal(){
		Worker vigilantWorker = Main.shop.getRandomWorker();
		if (confrontation(agility, vigilantWorker.getVision())) steal();
		else{
			vigilantWorker.addToCriminal(1);
		}
	}
}
