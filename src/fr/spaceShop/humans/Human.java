package fr.spaceShop.humans;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;

import fr.spaceShop.humans.actions.Steal;
import fr.spaceShop.main.Main;

public abstract class Human implements Steal, Serializable{
	protected String name;
	private static int nbHumans =0;
	private int id;
	
	//stats
	protected int criminal;
	protected int strenght;
	protected int charism;
	protected int agility;
	protected int vision;
	private static String names[] = null;
	
	
	public Human(){
		name = getRandomName();
		id = nbHumans++;
		criminal = (int) (Math.random() * ( 20 - 0 ));
		charism = (int) (Math.random() * ( 20 - 0 ));
		strenght = (int) (Math.random() * ( 20 - 0 ));
		agility = (int) (Math.random() * ( 20 - 0 ));
		vision = (int) (Math.random() * ( 20 - 0 ));
	}
	
	public Human(Human human){
		name = human.getName();
		id = human.getId();
		criminal = human.getCriminal();
		charism = human.getCharism();
		strenght = human.getStrenght();
		agility = human.getAgility();
		vision = human.getVision();
	}
	
	//accessor
	public int getCriminal(){
		return criminal;
	}
	
	public int getCharism(){
		return charism;
	}
	
	public int getStrenght(){
		return strenght;
	}
	
	public int getAgility(){
		return agility;
	}
	
	public int getVision(){
		return vision;
	}
	
	public String getName(){
		return name;
	}
	
	public int getId() {
		return id;
	}
	
	//return winner 
	public static boolean confrontation(int stat0, int stat1){
		if (((int) (Math.random() * ( 20 - 0 )))< stat0 + (stat1-10)) return true; 
		else return false;
	}
	
	public String toString(){
		return "*******************\nJe m'appelle " + name + " j'ai pour id " + id + " j'ai les stats suivant :\n-criminalité : " + criminal + "\n-charisme : " + charism + "\n-force : " + strenght + "\n-agilité : " + agility + "\n-vision : " + vision + "\n*******************\n"; 
	}

	public boolean wantSteal() {
		if (((int)Math.random()* (20 - 0)) < criminal) return true;
		else return false;
	}

	public void steal() {
		if(Main.shop.getMoney()>10)Main.shop.addToMoney((int) -(Math.random() * ( 10 - 0)));
		else Main.shop.addToMoney((int) -(Math.random() * (Main.shop.getMoney() - 0)));
	}
	
	public void trySteal(){
		//type of vigilant persone
		Worker vigilantPersonn;
		int guardBonus = 0;
		if(Main.shop.thereGuard()) {
			vigilantPersonn = Main.shop.getRandomGuard();
			guardBonus = 4;
		}
		else vigilantPersonn = Main.shop.getRandomWorker();
		
		//if the vigilant is this personn 
		if(vigilantPersonn.equals(this)) steal();
		
		//confrontation
		if (confrontation(agility, vigilantPersonn.getVision()+guardBonus)) steal();
		else {
			if(vigilantPersonn.catchThief(this)){
				Main.shop.alert(this);
			}
		}
	}
	
	public static void init(){
		ObjectInputStream ois;
		try {
    		ois = new ObjectInputStream(
    	              new BufferedInputStream(
    	                new FileInputStream(
    	                  new File("files/names"))));
    	      try {
    	    	  names =(String[]) ois.readObject();
    	      } catch (ClassNotFoundException e) {
    	        e.printStackTrace();
    	      }
    	      ois.close();
    	    } catch (FileNotFoundException e) {
    	      e.printStackTrace();
    	    } catch (IOException e) {
    	      e.printStackTrace();
    	    }  
	}
	
	public String getRandomName(){
		return names[(int) (Math.random() * ( names.length ))];
	}

	abstract public String getJob();

}
