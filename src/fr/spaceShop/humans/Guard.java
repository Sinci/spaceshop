package fr.spaceShop.humans;

import fr.spaceShop.main.Main;

public class Guard extends Worker{
	
	public Guard(){
		super();
	}
	
	public Guard(Worker worker){
		super(worker);
	}

	
	public void trySteal(){
		Worker vigilantWorker = Main.shop.getRandomWorker();
		if (confrontation(agility, vigilantWorker.getVision())) steal();
		else{
			Main.shop.alert(this);
		}
	}
	
	public boolean catchThief(Human thief) {
		return confrontation(((strenght + agility) /2) + 4, (thief.getStrenght() + thief.getAgility()) /2);
	}
	
	public String getJob() {
		return "Guard";
	}
}
