package fr.spaceShop.humans;

import fr.spaceShop.main.Main;


public class Customer extends Human{
	private int fidelity;
	private static int aForEquation = 20;
	private double bForEquation;
	
	
	public Customer(){
		super();
		fidelity = 1;
		bForEquation  = -1 * (Math.random() * (0.1));
		
	}
	
	public String getJob() {
		return "Customer";
		
	}
	
	public int getFidelity(){
		return fidelity;
	}
	
	public void buy(){
		//Product product = Main.shop.getRandomProduct();
		//if(Main.shop.haveInStock(product) && ((bForEquation * product.getPrice()+20)>(Math.random() * (20)))) { //do;
		if(Main.shop.thereSeller()){
			if(fidelity<20)fidelity += Main.shop.getRandomSeller().sell(this/*,product*/);
			else Main.shop.getRandomSeller().sell(this/*,product*/);
		}
		else Main.shop.getRandomWorker().sell(this/*,product*/);
	}

	
}
