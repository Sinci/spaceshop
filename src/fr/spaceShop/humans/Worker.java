package fr.spaceShop.humans;

import fr.spaceShop.globalInformations.Date;
import fr.spaceShop.humans.actions.BeOnGuard;
import fr.spaceShop.humans.actions.Sell;
import fr.spaceShop.main.Main;
import fr.spaceShop.shop.Shop;

public class Worker extends Human implements Sell, BeOnGuard{
	private int salary;
	protected int hapinness;
	private Date firstDayDate;
	private PersonalInformations personalInformations;
	private int level;
	
	
	public Worker(){
		super();
		firstDayDate = Date.currentDate;
		personalInformations = new PersonalInformations(name);;
		calculateLevel();
		calculateSalary();
	}
	
	public Worker(Worker worker){
		super(worker);
		salary = worker.getSalary();
		hapinness = worker.getHapiness();
		firstDayDate = worker.getFirstDayDate();
		personalInformations = worker.getPersonalInformations();
		level = worker.getLevel();
		
	}
	
	public int getHapiness(){
		return hapinness;
	}
	
	public PersonalInformations getPersonalInformations(){
		return personalInformations;
	}
	
	public int getLevel(){
		return level;
	}
	
	public Date getFirstDayDate(){
		return firstDayDate;
	}
	
	public void addToCriminal(int i){
		criminal += i;
	}
	
	public void calculateLevel(){
		level = agility + charism + vision + strenght;
	}
	public void calculateSalary(){
		if(level<10) salary = 50;
		else if (level<30) salary = 150;
		else if (level<40) salary = 200;
		else if (level<60) salary = 400;
		else salary = 800;
	}
	
	public boolean viewThief(Human thief) {
		
		return false;
	}
	public int sell(Customer customer) { //think to remove product from stocks
		//get a random product from preferences
		int price = 10;
		//money of customer's purchase with loss if seller is clumsy
		if(vision < ((int) (Math.random() * ( 20 - 0 )))) Main.shop.addToMoney(price - ( (float) (Math.random() * ( price - 0 ))));
		else Main.shop.addToMoney(price);
		//return a bonus for fidelity if seller is hapiness
		if(hapinness < ((int) (Math.random() * ( 20 - 0 )))) return 1;
		else return -1;
	}
	
	public boolean viewThief(int id) {
		return false;
	}
	public boolean viewThief() {
		return false;
	}

	public String getJob() {
		return "Worker";
	}
	
	public boolean catchThief(Human thief) {
		return confrontation((strenght + agility) /2, (thief.getStrenght() + thief.getAgility()) /2);
	}

	public int getSalary() {
		return salary;
	}

	public void setSalary(int salary) {
		this.salary = salary;
	}
	
	public void candidate(){ //sometimes we add a worker never have guard 
		//gui ask
		int rp = ((int) (Math.random() * ( 2 - 0 )));
		if (rp == 0){
			Main.shop.addWorker(this.toGuard());
		}
		else if (rp == 1){
			Main.shop.addWorker(this.toSeller());
		}
	}
	
	public Guard toGuard(){
		return new Guard(this);
	}
	
	public Seller toSeller(){
		return new Seller(this);
	}
	
}
