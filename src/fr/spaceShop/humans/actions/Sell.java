package fr.spaceShop.humans.actions;

import fr.spaceShop.humans.Customer;
import fr.spaceShop.shop.Shop;

public interface Sell {
	public int sell(Customer customer);
}
