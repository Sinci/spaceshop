package fr.spaceShop.humans.actions;

import fr.spaceShop.humans.Human;

public interface BeOnGuard {
	
	public boolean viewThief();
	public boolean viewThief(int id);
	public boolean catchThief(Human thief);
	
}
