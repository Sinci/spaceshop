package fr.spaceShop.humans.actions;

import fr.spaceShop.shop.Shop;

public interface Steal {
	
	public boolean wantSteal();
	
	public void steal();
	
	public void trySteal();
}
