package fr.spaceShop.humans;

import fr.spaceShop.main.Main;

public class Seller extends Worker{
	
	public Seller(){
		super();
	}
	
	public Seller(Worker worker){
		super(worker);
	}
	
	public int sell(Customer customer) { //think to remove product from stocks
		//get a random product from preferences
		float price = 10;
		//money of customer's purchase with loss if seller is clumsy
		if(vision -2 < ((int) (Math.random() * ( 20 - 0 )))) Main.shop.addToMoney(price - ( (float) (Math.random() * ( price - 0 ))));
		else Main.shop.addToMoney(price);
		//return a bonus for fidelity if seller is hapiness
		if(hapinness -2 < ((int) (Math.random() * ( 20 - 0 )))) return 1;
		else return -1;
	}

	public String getJob() {
		return "Seller";
	}
}
