package fr.spaceShop.time;

import fr.spaceShop.globalInformations.Date;
import fr.spaceShop.main.Main;



public class Time {
	
	public static void gameLoop(){
		for(int i =0; i <10; i++){
			//random things
			if(((int) (Math.random() * (20))) ==0) Events.randomEvent();
			for(int j =0; j <Main.shop.getCustomersSize() ; j++){
				if(Main.shop.getCustomer(j).getFidelity() < (int) (Math.random() * ( 20 - 0 ))){
					if(Main.shop.getCustomer(j).wantSteal()) Main.shop.getCustomer(j).trySteal();
					Main.shop.getCustomer(j).buy();
				}
			}
			for(int j =0; j <Main.shop.getShopNbGuards() ; j++){
				if(Main.shop.getGuard(j).wantSteal()) Main.shop.getGuard(j).trySteal();
			}
			for(int j =0; j <Main.shop.getNbSellers() ; j++){
				if(Main.shop.getSeller(j).wantSteal()) Main.shop.getSeller(j).trySteal();
			}
			System.out.println(Date.currentDateToString());
			System.out.println(Main.shop.describe());
			if(Date.nextDay())endMonth();
		}
	}
	
	private static void endMonth(){
		Main.shop.addToStatCurrentMoney();
		Main.shop.payTax();
		Main.shop.paySalries();
	}
}