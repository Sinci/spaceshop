package fr.spaceShop.time;

import fr.spaceShop.humans.Customer;
import fr.spaceShop.humans.Worker;
import fr.spaceShop.main.Main;

public class Events {

	public static void randomEvent() {
		int randomThings = (int) (Math.random() * (100));
		if (randomThings < 40){
			Worker worker = new Worker();
			worker.candidate();
		}
		else if (randomThings < 80){
			Customer customer = new Customer();
			Main.shop.addCustomer(customer);
		}
	}

}
