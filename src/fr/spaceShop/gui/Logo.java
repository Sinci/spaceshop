package fr.spaceShop.gui;

import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class Logo extends JPanel{
	public void paintComponent(Graphics g){
		try{
			Image img = ImageIO.read(new File("files/img/logo.png"));
	    	g.drawImage(img, 0, 0, 800, 200, this);
	   	} catch (IOException e) {
	   		e.printStackTrace();
	   	}
	}
	
	
}
