package fr.spaceShop.gui;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Button extends JPanel{
	
	private JLabel title, content;
	private JButton button;
	
	public Button(String text, String link){
		title = new JLabel(text);
		button = new JButton(new ImageIcon(link));
		
		
		this.setPreferredSize(new Dimension(800, 200));
	    this.setOpaque(false);
	    this.setLayout(new GridBagLayout());
		
		
		GridBagConstraints gbc = new GridBagConstraints();

	    gbc.gridwidth = 1;
	    gbc.gridheight = 1;
	    
	    gbc.gridy = 0;
	    gbc.gridx = 0;
	    this.add(title, gbc);
	    gbc.gridy = 1;
	    this.add(button, gbc);
	}
	
}
