package fr.spaceShop.gui;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import fr.spaceShop.main.Main;


public class Window  extends JFrame{ 
	JPanel container = new JPanel();
	Background background = new Background();
	private int width;
	private int height;
	private JButton newGame = new JButton(new ImageIcon("files/img/newGame.png"));
	private Logo logo = new Logo();
	private MenuListener menuListener = new MenuListener();
	private JButton loadGame = new JButton(new ImageIcon("files/img/loadGame.png"));
	
	public Window() {
		this.add(background);
		//this.add(container);
		this.setTitle("Space Shop");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setUndecorated(true);
        width = (int)Toolkit.getDefaultToolkit().getScreenSize().width;
		height = (int)Toolkit.getDefaultToolkit().getScreenSize().height; 
        this.setSize(width, height);
        this.setVisible(true);
        menu();
	}
	
	public void menu(){ //remove unused block
		
	    //there is a game in files ?
		ObjectInputStream ois;
		boolean isAlreadyGameFile = false;
		try {
    		ois = new ObjectInputStream(
    	              new BufferedInputStream(
    	                new FileInputStream(
    	                  new File("files/isGame"))));
    	            

    	      try {
    	    	  isAlreadyGameFile =(Boolean)ois.readObject();
    	      } catch (ClassNotFoundException e) {
    	        e.printStackTrace();
    	      }
    	      ois.close();
    	    } catch (FileNotFoundException e) {
    	      e.printStackTrace();
    	} catch (IOException e) {
	      e.printStackTrace();
	    } 
		
		
	    //logo
	    JPanel logoMargeEast = new JPanel();
	    logoMargeEast.setPreferredSize(new Dimension((width-800)/4, 200)); //why /4 and not /2 ?
	    logoMargeEast.setOpaque(false);
	    
	    JPanel logoMargeWest = new JPanel();
	    logoMargeWest.setPreferredSize(new Dimension((width-800)/4, 200)); //why /4 and not /2 ?
	    logoMargeWest.setOpaque(false);
	    logo.setPreferredSize(new Dimension(800,200));
	    
	    //LogoBottomMarge
	    JPanel logoBottomMarge = new JPanel();
	    logoBottomMarge.setPreferredSize(new Dimension(800, (height-600)/4)); //whey /4 and not /2 ?
	    logoBottomMarge.setOpaque(false);
		
		JPanel logoBottomMargeEast = new JPanel();
		logoBottomMargeEast.setPreferredSize(new Dimension((width-800)/4, (height-600)/4)); //whey /4 and not /2 ?
		logoBottomMargeEast.setOpaque(false);
	    
	    JPanel logoBottomMargeWest = new JPanel();
	    logoBottomMargeWest.setPreferredSize(new Dimension((width-800)/4, (height-600)/4)); //whey /4 and not /2 ?
	    logoBottomMargeWest.setOpaque(false);
	    
	    
	    //newGame
	    JPanel newGameMargeEast = new JPanel();
	    newGameMargeEast.setPreferredSize(new Dimension((width-800)/4, 200)); //whey /4 and not /2 ?
	    newGameMargeEast.setOpaque(false);
	    
	    JPanel newGameMargeWest = new JPanel();
	    newGameMargeWest.setPreferredSize(new Dimension((width-800)/4, 200)); //whey /4 and not /2 ?
	    newGameMargeWest.setOpaque(false);
	    
	    newGame.addActionListener(menuListener);
	    newGame.setPreferredSize(new Dimension(800,200));
	    
	    //newGameBottomMarge
	    JPanel newGameBottomMarge = new JPanel();
	    newGameBottomMarge.setPreferredSize(new Dimension(800, (height-600)/4)); //whey /4 and not /2 ?
	    newGameBottomMarge.setOpaque(false);
		
		JPanel newGameBottomMargeEast = new JPanel();
		newGameBottomMargeEast.setPreferredSize(new Dimension((width-800)/4, (height-600)/4)); //whey /4 and not /2 ?
		newGameBottomMargeEast.setOpaque(false);
	    
	    JPanel newGameBottomMargeWest = new JPanel();
	    newGameBottomMargeWest.setPreferredSize(new Dimension((width-800)/4, (height-600)/4)); //whey /4 and not /2 ?
	    newGameBottomMargeWest.setOpaque(false);
	    
	  //loadGame
	    JPanel loadGameMargeEast = new JPanel();
	    loadGameMargeEast.setPreferredSize(new Dimension((width-800)/4, 200)); //whey /4 and not /2 ?
	    loadGameMargeEast.setOpaque(false);
	    
	    JPanel loadGameMargeWest = new JPanel();
	    loadGameMargeWest.setPreferredSize(new Dimension((width-800)/4, 200)); //whey /4 and not /2 ?
	    loadGameMargeWest.setOpaque(false);
	    
	    loadGame.addActionListener(menuListener);
	    loadGame.setEnabled(isAlreadyGameFile);
	    loadGame.setPreferredSize(new Dimension(800,200));
	    
	    //loadGameBottomMarge
	    JPanel loadGameBottomMarge = new JPanel();
	    loadGameBottomMarge.setPreferredSize(new Dimension(800, (height-600)/4)); //whey /4 and not /2 ?
	    loadGameBottomMarge.setOpaque(false); //change color of button where it's disable
		
		JPanel loadGameBottomMargeEast = new JPanel();
		loadGameBottomMargeEast.setPreferredSize(new Dimension((width-800)/4, (height-600)/4)); //whey /4 and not /2 ?
		loadGameBottomMargeEast.setOpaque(false);
	    
	    JPanel loadGameBottomMargeWest = new JPanel();
	    loadGameBottomMargeWest.setPreferredSize(new Dimension((width-800)/4, (height-600)/4)); //whey /4 and not /2 ?
	    loadGameBottomMargeWest.setOpaque(false);
	   
	    //content
	    JPanel content = new JPanel();
	    content.setPreferredSize(new Dimension(width, height));
	    content.setOpaque(false);
	    content.setLayout(new GridBagLayout());

	    
	    GridBagConstraints gbc = new GridBagConstraints();

	        
	    gbc.gridx = 0;
	    gbc.gridy = 0;
	    gbc.gridwidth = 1;
	    gbc.gridheight = 1;
	    
	   //logo
	    gbc.gridy = 0;
	    gbc.gridx = 0;
	    content.add(logoMargeEast, gbc);
	    gbc.gridx = 1;
	    content.add(logo, gbc);
	    gbc.gridx = 2;
	    content.add(logoMargeWest, gbc);
	    
	    //logoBottomMargeWest
	    gbc.gridy = 1;
	    gbc.gridx = 0;
	    content.add(logoBottomMargeEast, gbc);
	    gbc.gridx = 1;
	    content.add(logoBottomMarge, gbc);
	    gbc.gridx = 2;
	    content.add(logoBottomMargeWest, gbc);
	    
	    //newGame
	    gbc.gridy = 2;
	    gbc.gridx = 0;
	    content.add(newGameMargeEast, gbc);
	    gbc.gridx = 1;
	    content.add(newGame, gbc);
	    gbc.gridx = 2;
	    content.add(newGameMargeWest, gbc);
	    
	    //newGameBottomMargeWest
	    gbc.gridy = 3;
	    gbc.gridx = 0;
	    content.add(newGameBottomMargeEast, gbc);
	    gbc.gridx = 1;
	    content.add(newGameBottomMarge, gbc);
	    gbc.gridx = 2;
	    content.add(newGameBottomMargeWest, gbc);
	    
	    //loadGame
	    gbc.gridy = 4;
	    gbc.gridx = 0;
	    content.add(loadGameMargeEast, gbc);
	    gbc.gridx = 1;
	    content.add(loadGame, gbc);
	    gbc.gridx = 2;
	    content.add(loadGameMargeWest, gbc);
	    
	    //loadGameBottomMargeWest
	    gbc.gridy = 5;
	    gbc.gridx = 0;
	    content.add(loadGameBottomMargeEast, gbc);
	    gbc.gridx = 1;
	    content.add(loadGameBottomMarge, gbc);
	    gbc.gridx = 2;
	    content.add(loadGameBottomMargeWest, gbc);
	    background.add(content);
	}	
	
	class MenuListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			if(e.getSource() == loadGame) Main.load();
			else Main.newGame();
		}
	}
	
	
}
