package fr.spaceShop.shop;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import fr.spaceShop.humans.Customer;
import fr.spaceShop.humans.Guard;
import fr.spaceShop.humans.Human;
import fr.spaceShop.humans.PDG;
import fr.spaceShop.humans.Seller;
import fr.spaceShop.humans.Worker;

public class Shop implements Serializable { //change  to nothing
	private String name;
	private List<Customer> customers = new ArrayList();
	private List<Worker> guards = new ArrayList();
	private List<Worker> sellers = new ArrayList();
	private Worker pdg = new PDG();
	private int nbGuard, nbSeller;
	private int money;
	private List<Integer> statMoneyEarn = new ArrayList();
	private List<Integer> statTax = new ArrayList();

	// list stocks
	// list ventes


	public Shop() {
		nbGuard = 0;
		System.out.println(thereGuard());
		nbSeller = 0;
		money = 200;
		name = "Space Shop";
		addToStatCurrentMoney();
		addToStatTaxt(0);
	}

	public boolean thereGuard() {
		return (nbGuard > 0);
	}

	public boolean thereSeller() {
		return (nbSeller > 0);
	}


	public int getMoney() {
		return this.money;
	}

	public int getNbGuards() {
		return nbGuard;
	}

	public int getShopNbGuards() {
		return getNbGuards();
	}

	public int getNbSellers() {
		return nbSeller;
	}

	public void setNbGuards(int i) {
		nbGuard = i;
	}

	public  void addToMoney(float x) {
		money += x;
	}

	public  Guard getRandomGuard() {
		return (Guard) guards.get((int) Math.random() * (nbGuard));
	}

	public  Worker getRandomWorker() {
		int random = (int) Math.random() * (nbGuard + nbSeller + 1);
		if (random < nbGuard)
			return guards.get(random);
		else if (random < nbSeller)
			return sellers.get(random);
		else
			return pdg;
	}

	public  Seller getRandomSeller() {
		return (Seller) sellers.get((int) Math.random()
				* (sellers.size()));
	}

	public  Seller getSeller(int i) {
		return (Seller) sellers.get(i);
	}

	public  Guard getGuard(int i) {
		return (Guard) guards.get(i);
	}

	public  void putToDeathCustomer(int id) {
		for (int i = 0; i < customers.size(); i++) {
			if (customers.get(i).getId() == id) {
				customers.remove(i);
				break;
			}
		}
	}

	public  void fire(int id) {
		for (int i = 0; i < sellers.size(); i++) {
			if (sellers.get(i).getId() == id) {
				sellers.remove(i);
				break;
			}
		}
		for (int i = 0; i < guards.size(); i++) {
			if (guards.get(i).getId() == id) {
				guards.remove(i);
				break;
			}
		}
	}

	public  void alert(Human human) {
		System.out.println(human.getName() + " est un voleur !");

	}

	public  void addCustomer(Customer customer) {
		customers.add(customer);
	}

	public  void addWorker(PDG pdg) {
		pdg = pdg;
	}

	public  void addWorker(Guard guard) {
		guards.add(guard);
		nbGuard++;
	}

	public  void addWorker(Seller seller) {
		sellers.add(seller);
		nbSeller++;
	}

	public  int getCustomersSize() {
		return customers.size();
	}

	public  Customer getCustomer(int i) {
		return customers.get(i);
	}

	public  String describe() {
		String str = name + "\n";
		for (int i = 0; i < customers.size(); i++)
			str += "Client " + i + " " + customers.get(i).getName() + "\n";
		for (int i = 0; i < sellers.size(); i++)
			str += "vendeur " + i + " " + sellers.get(i).getName() + "\n";
		for (int i = 0; i < guards.size(); i++)
			str += "garde " + i + " " + guards.get(i).getName() + "\n";
		str += "pdg " + pdg.getName() + "\n";
		str += getMoney() + "\n\n\n";
		return str;

	}

	public  void paySalries() {
		for (int i = 0; i < sellers.size(); i++) {
			if (money >= sellers.get(i).getSalary())
				money -= sellers.get(i).getSalary();
			else
				System.out.println("Game over !");
		}
		for (int i = 0; i < guards.size(); i++) {
			if (money >= guards.get(i).getSalary())
				money -= guards.get(i).getSalary();
			else
				System.out.println("Game over !");
		}
		if (money >= pdg.getSalary()) {
			int pdgSalary = pdg.getSalary();
			PDG.addToMoney(pdgSalary);
			money -= pdgSalary;
		} else
			PDG.addToMoney(money);
	}

	public  void payTax() {
		int earn = statMoneyEarn.get(statMoneyEarn.size() - 1)
				- statMoneyEarn.get(statMoneyEarn.size() - 2)
				- statTax.get(statTax.size() - 1);// - tax;
		if (earn > 0)
			money -= earn / 4;
		addToStatTaxt(earn / 4);

	}

	public  void addToStatCurrentMoney() {
		statMoneyEarn.add(money);
	}

	public  void addToStatTaxt(int tax) {
		statTax.add(tax);
	}

	public  void load() {
		/*
	private  int nbGuard, nbSeller;
	private  int money;
	private  List<Integer> statMoneyEarn = new ArrayList();
	private  List<Integer> statTax = new ArrayList();
		 * 
		 * 
		 * 
		 */	
		
	}

}
